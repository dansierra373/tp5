from app import compute
import pytest


def test_simple_add():
    assert compute(3, 5, "+") == 8


def test_simple_minus():
    assert compute(3, 5, "-") == -2


@pytest.mark.skip(reason="To implement later")
def test_simple_multiply():
    assert compute(3, 5, "*") == 15


def test_error():
    with pytest.raises(ValueError):
        assert compute(3, 5, "$") == -2
