import os

import requests

URL = os.environ.get("API_URL", "http://localhost:5000")


def auth(session):
    r = session.post(f"{URL}/api/login",
                     json={
                         "username": "Admin",
                         "password": "admin"
                     })
    r.raise_for_status()


def test_compute():
    session = requests.session()
    auth(session)
    r = session.get(f"{URL}/api/compute", params={
        "a": 10,
        "b": 5,
        "operator": "+"
    })
    r.raise_for_status()
    assert r.json()["result"] == 15


def test_compute_negative():
    session = requests.session()
    auth(session)
    r = session.get(f"{URL}/api/compute", params={
        "a": "10",
        "b": "-5",
        "operator": "+"
    })
    r.raise_for_status()
    assert r.json()["result"] == 5
