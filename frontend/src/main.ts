import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";

import './assets/main.css'

import { createApp } from 'vue'
import { createStore } from 'vuex'

import App from './App.vue'
import router from './router'

// Create a new store instance.
const store = createStore({
    state () {
      return {
        logged: false,
        username: ""
      }
    },
  })
  

const app = createApp(App)
app.provide("$store", store);
app.config.globalProperties.$store = store;
app.use(store)
app.use(router)
app.mount('#app')




